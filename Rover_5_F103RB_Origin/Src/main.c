/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2020 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "adc.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include <stdlib.h>
#include <stdio.h>
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

#define ADDR 3

#define AVANCE 	GPIO_PIN_SET
#define RECULE  GPIO_PIN_RESET
#define POURCENT 640
#define Seuil_Dist_4 1600 // corespond � 10 cm.
#define Seuil_Dist_3 1600
#define Seuil_Dist_1 1600
#define Seuil_Dist_2 1600
#define V1 38 //10 cm/s
#define V2 56 //20 cm/s
#define V3 76 //30 cm/s
#define Vmax 95
#define T_2_S 1000 //( pwm p�riode = 2 ms )
#define T_200_MS 100
#define T_2000_MS 1000
#define CKp_D 100  //80 Robot1
#define CKp_G 100  //80 Robot1
#define CKi_D 80  //50 Robot1
#define CKi_G 80  //50 Robot1
#define CKd_D 0
#define CKd_G 0
#define DELTA 0x50

#define T_TOURNER_90_1 2000 //V2 = 2400
#define T_TOURNER_90_2 1400 //V2 = 2400
#define T_TOURNER_SONAR 1200
#define MARGE_Y 3000 //15cm
#define MARGE_X 6500 //40cm
#define MARGE_Z 2000 //20cm
#define MARGE_ALIGNEMENT 4000 //40cm
#define ERREUR_ALIGNEMENT 1000

#define NB_ROBOT 10

enum CMDE {
	MOV_PARK,
	DMD_ADDR,
	SEND_MOV_PARK,
	PARK,
	CMD_ATTENTE_PARK,
	START,
	STOP,
	AVANT,
	ARRIERE,
	DROITE,
	GAUCHE
};
volatile enum CMDE CMDE;
enum MODE {
	SLEEP, ACTIF
};
volatile enum MODE Mode;

enum ETAT_PARK {
	DEBUT,
	ATTENTE_PARK,
	AVANCER_MESURE,
	ENREGISTREMENT_MESURE_X,
	ENREGISTREMENT_MESURE_Z,
	LANCEMENT_MESURE_Z,
	LANCEMENT_MESURE_X,
	TOURNER_SERVO_p90,
	TOURNER_SERVO_m90,
	TOURNER_ROB,
	AVANCER_ALIGNEMENT,
	TOURNER_PLACE,
	TOURNER_SERVO_0,
	LANCEMENT_MESURE_Y,
	ENREGISTREMENT_MESURE_Y,
	AVANCER_PLACE,
	PARKING,
	TOURNER_SERVO_POS_Z,
	ENREGISTREMENT_MESURE_POS_Z,
	LANCEMENT_MESURE_POS_Z,
	FIN,
	PARKED,
	REINIT
};
enum ETAT_PARK etat_park = DEBUT;
volatile unsigned char New_CMDE = 0;
volatile uint16_t Dist_ACS_1, Dist_ACS_2, Dist_ACS_3, Dist_ACS_4;
volatile unsigned int Time = 0;
volatile unsigned int Tech = 0;
uint16_t adc_buffer[10];
uint16_t Buff_Dist[8];
uint8_t BLUE_RX;

uint16_t _DirG, _DirD, CVitG, CVitD, DirD, DirG;
uint16_t _CVitD = 0;
uint16_t _CVitG = 0;
uint16_t VitD, VitG, DistD, DistG;
uint16_t DistD_old = 0;
uint16_t DistG_old = 0;
int Cmde_VitD = 0;
int Cmde_VitG = 0;
unsigned long Dist_parcours = 0;
volatile uint32_t Dist_Obst;
uint32_t Dist_Obst_;
uint32_t Dist_Obst_cm;
uint32_t Dist;
uint8_t UNE_FOIS = 1;
uint32_t OV = 0;

volatile uint8_t fin_lect_sonar = 0;
volatile uint32_t dist_sonar = 0;
uint32_t target_x, target_y, target_z;
uint32_t pos_x, pos_y, pos_z;
uint32_t tmp_park = 0;
uint32_t tmp_seuil = 0;
uint8_t	sens = 0;

volatile uint8_t zigbee_RX;
volatile uint8_t i_rx_pos = 0;
uint8_t target_addr = 0;

volatile uint8_t addr_array[NB_ROBOT] = {0};
volatile uint8_t i_addr_rx;

uint32_t mesures[5];
uint8_t nb_mesure=0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_NVIC_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void Gestion_Commandes(void);
void regulateur(void);
void controle(void);
void Calcul_Vit(void);
void ACS(void);
void lecture_sonar();
void setAngleSonar(int angle);
void PARK_ASSIST(void);
void zig_tx_send(uint8_t buffer);
uint8_t mini(uint8_t* tab, uint8_t longueur);
uint32_t Moyenne(uint32_t* tab, uint8_t taille);
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */

	HAL_Init();

  /* USER CODE BEGIN Init */
  Dist_Obst = 0;
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_USART3_UART_Init();
  MX_TIM1_Init();
  MX_USART1_UART_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */
  	HAL_SuspendTick(); // suppresion des Tick interrupt pour le mode sleep.

  	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_4);  // Start PWM motor
  	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
  	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_4);
  	CMDE = STOP;
  	New_CMDE = 1;
  	HAL_TIM_Base_Start_IT(&htim2);  // Start IT sur font montant PWM
  	HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);
  	HAL_TIM_Encoder_Start(&htim4, TIM_CHANNEL_ALL);
  	HAL_UART_Receive_IT(&huart3, &BLUE_RX, 1);
  	HAL_UART_Receive_IT(&huart1, &zigbee_RX, 1);
  	HAL_ADC_Start_IT(&hadc1);
  	HAL_TIM_IC_Start_IT (&htim1, TIM_CHANNEL_2);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  	setAngleSonar(0);
  	target_z = 10000; //100cm
  	lecture_sonar();
  while (1)
  {
	  Gestion_Commandes();
	  controle();
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV8;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
  /* TIM1_CC_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM1_CC_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(TIM1_CC_IRQn);
  /* TIM2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(TIM2_IRQn);
  /* EXTI15_10_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
}

/* USER CODE BEGIN 4 */
void Gestion_Commandes(void) {
	enum ETAT {
		STATE_MOV_PARK,
		STATE_ATTENTE_PARK,
		VEILLE,
		ARRET,
		AV1,
		AV2,
		AV3,
		RV1,
		RV2,
		RV3,
		DV1,
		DV2,
		DV3,
		GV1,
		GV2,
		GV3
	};
	static enum ETAT Etat = VEILLE;

if (New_CMDE) {
		New_CMDE = 0;
	switch (CMDE) {
		case STOP: {
			_CVitD = _CVitG = 0;
			// Mise en sommeil: STOP mode , r�veil via IT BP1
			Etat = VEILLE;
			Mode = SLEEP;
			etat_park = DEBUT;
			break;
		}
		case START: {
			// r�veil syt�me grace � l'IT BP1
			Etat = ARRET;
			Mode = SLEEP;
			etat_park = DEBUT;
			break;
		}
		case DMD_ADDR:
		{
			if (Etat == STATE_ATTENTE_PARK)
			{
				zig_tx_send(ADDR);
			}
			break;
		}
		case PARK:
		{
			_DirG = AVANCE;
			_DirD = AVANCE;
			_CVitG = 0;
			_CVitD = 0;
			etat_park = TOURNER_SERVO_POS_Z;;
			Etat=ARRET;
			Mode = ACTIF;
			break;
		}
		case MOV_PARK:
		{
			if (Etat == STATE_ATTENTE_PARK)
			{
				Etat = STATE_MOV_PARK;
				etat_park = AVANCER_MESURE; //Provisoir en attendant d'avoir zigbee fonctionnel
				Mode = ACTIF;
			}
			break;
		}
		case SEND_MOV_PARK:
		{
			zig_tx_send(target_addr);
			zig_tx_send(pos_z%256);
			zig_tx_send((pos_z/256)%256);
			zig_tx_send((pos_z/(256*256))%256);
			zig_tx_send('M');
			break;
		}
		case CMD_ATTENTE_PARK:
		{
			Etat = STATE_ATTENTE_PARK;
			etat_park = ATTENTE_PARK;
			_DirG = AVANCE;
			_DirD = AVANCE;
			_CVitG = 0;
			_CVitD = 0;
			Mode = ACTIF;
			break;
		}
		case AVANT: {
			switch (Etat) {
			case STATE_MOV_PARK:
			{
				if(Mode==SLEEP){
					_DirG = AVANCE;
					_DirD = AVANCE;
					_CVitG = V1;
					_CVitD = V1;
					Etat = AV1;
					Mode = ACTIF;
				}
				break;
			}
			case VEILLE: {
				Etat = VEILLE;
				Mode = SLEEP;
				break;
			}
			case ARRET: {
				_DirG = AVANCE;
				_DirD = AVANCE;
				_CVitG = V1;
				_CVitD = V1;
				Etat = AV1;
				Mode = ACTIF;
				break;
			}
			case AV1: {
				_DirG = AVANCE;
				_DirD = AVANCE;
				_CVitG = V2;
				_CVitD = V2;
				Etat = AV2;
				Mode = ACTIF;
				break;
			}
			case AV2: {
				_DirG = AVANCE;
				_DirD = AVANCE;
				_CVitG = V3;
				_CVitD = V3 ;
				Etat = AV3;
				Mode = ACTIF;
				break;
			}
			case AV3: {
				_DirG = AVANCE;
				_DirD = AVANCE;
				_CVitG = V3;
				_CVitD = V3 ;
				Etat = AV3;
				Mode = ACTIF;
				break;
			}
			case RV1: {
				_DirG = RECULE;
				_DirD = RECULE;
				_CVitG = 0;
				_CVitD = 0;
				Etat = ARRET;
				Mode = SLEEP;

				break;
			}
			case RV2: {
				_DirG = RECULE;
				_DirD = RECULE;
				_CVitG = V1;
				_CVitD = V1;
				Etat = RV1;
				Mode = ACTIF;
				break;
			}
			case RV3: {
				_DirG = RECULE;
				_DirD = RECULE;
				_CVitG = V2;
				_CVitD = V2;
				Etat = RV2;
				Mode = ACTIF;
				break;
			}
			case DV1: {
				_DirG = AVANCE;
				_DirD = AVANCE;
				_CVitG = V1;
				_CVitD = V1;
				Etat = AV1;
				Mode = ACTIF;
				break;
			}
			case DV2: {
				_DirG = AVANCE;
				_DirD = AVANCE;
				_CVitG = V2;
				_CVitD = V2;
				Etat = AV2;
				Mode = ACTIF;
				break;
			}
			case DV3: {
				_DirG = AVANCE;
				_DirD = AVANCE;
				_CVitG = V3;
				_CVitD = V3;
				Etat = AV3;
				Mode = ACTIF;
				break;
			}
			case GV1: {
				_DirG = AVANCE;
				_DirD = AVANCE;
				_CVitG = V1;
				_CVitD = V1;
				Etat = AV2;
				Mode = ACTIF;
				break;
			}
			case GV2: {
				_DirG = AVANCE;
				_DirD = AVANCE;
				_CVitG = V2;
				_CVitD = V2;
				Etat = AV2;
				Mode = ACTIF;
				break;
			}
			case GV3: {
				_DirG = AVANCE;
				_DirD = AVANCE;
				_CVitG = V3;
				_CVitD = V3;
				Etat = AV3;
				Mode = ACTIF;
				break;
			}
			}
			break;
		}
		case ARRIERE: {
			switch (Etat) {
			case STATE_MOV_PARK:
			{
				if(Mode==SLEEP){
					_DirG = RECULE;
					_DirD = RECULE;
					_CVitG = V1;
					_CVitD = V1;
					Etat = RV1;
					Mode = ACTIF;
				}
				break;
			}
			case VEILLE: {
				Etat = VEILLE;
				Mode = SLEEP;
				break;
			}
			case ARRET: {
				_DirG = RECULE;
				_DirD = RECULE;
				_CVitG = V1;
				_CVitD = V1;
				Etat = RV1;
				Mode = ACTIF;
				break;
			}
			case AV1: {
				_DirG = AVANCE;
				_DirD = AVANCE;
				_CVitG = 0;
				_CVitD = 0;
				Etat = ARRET;
				Mode = SLEEP;

				break;
			}
			case AV2: {
				_DirG = AVANCE;
				_DirD = AVANCE;
				_CVitG = V1;
				_CVitD = V1;
				Etat = AV1;
				Mode = ACTIF;
				break;
			}
			case AV3: {
				_DirG = AVANCE;
				_DirD = AVANCE;
				_CVitG = V2;
				_CVitD = V2;
				Etat = AV2;
				Mode = ACTIF;
				break;
			}
			case RV1: {
				_DirG = RECULE;
				_DirD = RECULE;
				_CVitG = V2;
				_CVitD = V2;
				Etat = RV2;
				Mode = ACTIF;
				break;
			}
			case RV2: {
				_DirG = RECULE;
				_DirD = RECULE;
				_CVitG = V3;
				_CVitD = V3;
				Etat = RV3;
				Mode = ACTIF;
				break;
			}
			case RV3: {
				_DirG = RECULE;
				_DirD = RECULE;
				_CVitG = V3;
				_CVitD = V3;
				Etat = RV3;
				Mode = ACTIF;
				break;
			}
			case DV1: {
				_DirG = RECULE;
				_DirD = RECULE;
				_CVitG = V1;
				_CVitD = V1;
				Etat = RV1;
				Mode = ACTIF;
				break;
			}
			case DV2: {
				_DirG = RECULE;
				_DirD = RECULE;
				_CVitG = V2;
				_CVitD = V2;
				Etat = RV2;
				Mode = ACTIF;
				break;
			}
			case DV3: {
				_DirG = RECULE;
				_DirD = RECULE;
				_CVitG = V3;
				_CVitD = V3;
				Etat = RV3;
				Mode = ACTIF;
				break;
			}
			case GV1: {
				_DirG = RECULE;
				_DirD = RECULE;
				_CVitG = V1;
				_CVitD = V1;
				Etat = RV1;
				Mode = ACTIF;
				break;
			}
			case GV2: {
				_DirG = RECULE;
				_DirD = RECULE;
				_CVitG = V2;
				_CVitD = V2;
				Etat = RV2;
				Mode = ACTIF;
				break;
			}
			case GV3: {
				_DirG = RECULE;
				_DirD = RECULE;
				_CVitG = V3;
				_CVitD = V3;
				Etat = RV3;
				Mode = ACTIF;
				break;
			}
			}
			break;
		}
		case DROITE: {
			switch (Etat) {
			case STATE_MOV_PARK:
			{
				if(Mode==SLEEP){
					_DirG = AVANCE;
					_DirD = RECULE;
					_CVitG = V1;
					_CVitD = V1;
					Etat = DV1;
					Mode = ACTIF;
				}
				break;
			}
			case VEILLE: {
				Etat = VEILLE;
				Mode = SLEEP;
				break;
			}
			case ARRET: {
				_DirG = AVANCE;
				_DirD = RECULE;
				_CVitG = V1;
				_CVitD = V1;
				Etat = DV1;
				Mode = ACTIF;
				break;
			}
			case AV1: {
				_DirG = AVANCE;
				_DirD = RECULE;
				_CVitG = V1;
				_CVitD = V1;
				Etat = DV1;
				Mode = ACTIF;
				break;
			}
			case AV2: {
				_DirG = AVANCE;
				_DirD = RECULE;
				_CVitG = V2;
				_CVitD = V2;
				Etat = DV2;
				Mode = ACTIF;
				break;
			}
			case AV3: {
				_DirG = AVANCE;
				_DirD = RECULE;
				_CVitG = V3;
				_CVitD = V3;
				Etat = DV3;
				Mode = ACTIF;
				break;
			}
			case RV1: {
				_DirG = AVANCE;
				_DirD = RECULE;
				_CVitG = V1;
				_CVitD = V1;
				Etat = DV1;
				Mode = ACTIF;
				break;
			}
			case RV2: {
				_DirG = AVANCE;
				_DirD = RECULE;
				_CVitG = V2;
				_CVitD = V2;
				Etat = DV2;
				Mode = ACTIF;
				break;
			}
			case RV3: {
				_DirG = AVANCE;
				_DirD = RECULE;
				_CVitG = V3;
				_CVitD = V3;
				Etat = DV3;
				Mode = ACTIF;
				break;
			}
			case DV1: {
				_DirG = AVANCE;
				_DirD = RECULE;
				_CVitG = V2;
				_CVitD = V2;
				Etat = DV2;
				Mode = ACTIF;
				break;
			}
			case DV2: {
				_DirG = AVANCE;
				_DirD = RECULE;
				_CVitG = V3;
				_CVitD = V3;
				Etat = DV3;
				Mode = ACTIF;
				break;
			}
			case DV3: {
				_DirG = AVANCE;
				_DirD = RECULE;
				_CVitG = V3;
				_CVitD = V3;
				Etat = DV3;
				Mode = ACTIF;
				break;
			}
			case GV1: {
				_DirG = RECULE;
				_DirD = RECULE;
				_CVitG = 0;
				_CVitD = 0;
				Etat = ARRET;
				Mode = SLEEP;

				break;
			}
			case GV2: {
				_DirG = RECULE;
				_DirD = AVANCE;
				_CVitG = V1;
				_CVitD = V1;
				Etat = GV1;
				Mode = ACTIF;
				break;
			}
			case GV3: {
				_DirG = RECULE;
				_DirD = AVANCE;
				_CVitG = V2;
				_CVitD = V2;
				Etat = GV2;
				Mode = ACTIF;
				break;
			}
			}
			break;
		}
		case GAUCHE: {
			switch (Etat) {
			case STATE_MOV_PARK:
			{
				if(Mode==SLEEP){
					_DirG = RECULE;
					_DirD = AVANCE;
					_CVitG = V1;
					_CVitD = V1;
					Etat = GV1;
					Mode = ACTIF;
				}
				break;
			}
			case VEILLE: {
				Etat = VEILLE;
				Mode = SLEEP;
				break;
			}
			case ARRET: {
				_DirG = RECULE;
				_DirD = AVANCE;
				_CVitG = V1;
				_CVitD = V1;
				Etat = GV1;
				Mode = ACTIF;
				break;
			}
			case AV1: {
				_DirG = RECULE;
				_DirD = AVANCE;
				_CVitG = V1;
				_CVitD = V1;
				Etat = GV1;
				Mode = ACTIF;
				break;
			}
			case AV2: {
				_DirG = RECULE;
				_DirD = AVANCE;
				_CVitG = V2;
				_CVitD = V2;
				Etat = GV2;
				Mode = ACTIF;
				break;
			}
			case AV3: {
				_DirG = RECULE;
				_DirD = AVANCE;
				_CVitG = V3;
				_CVitD = V3;
				Etat = GV3;
				Mode = ACTIF;
				break;
			}
			case RV1: {
				_DirG = RECULE;
				_DirD = AVANCE;
				_CVitG = V1;
				_CVitD = V1;
				Etat = GV1;
				Mode = ACTIF;
				break;
			}
			case RV2: {
				_DirG = RECULE;
				_DirD = AVANCE;
				_CVitG = V2;
				_CVitD = V2;
				Etat = GV2;
				Mode = ACTIF;
				break;
			}
			case RV3: {
				_DirG = RECULE;
				_DirD = AVANCE;
				_CVitG = V3;
				_CVitD = V3;
				Etat = GV3;
				Mode = ACTIF;
				break;
			}
			case DV1: {
				_DirG = RECULE;
				_DirD = RECULE;
				_CVitG = 0;
				_CVitD = 0;
				Etat = ARRET;
				Mode = SLEEP;

				break;
			}
			case DV2: {
				_DirG = AVANCE;
				_DirD = RECULE;
				_CVitG = V1;
				_CVitD = V1;
				Etat = DV1;
				Mode = ACTIF;
				break;
			}
			case DV3: {
				_DirG = AVANCE;
				_DirD = RECULE;
				_CVitG = V2;
				_CVitD = V2;
				Etat = DV2;
				Mode = ACTIF;
				break;
			}
			case GV1: {
				_DirG = RECULE;
				_DirD = AVANCE;
				_CVitG = V2;
				_CVitD = V2;
				Etat = GV2;
				Mode = ACTIF;
				break;
			}
			case GV2: {
				_DirG = RECULE;
				_DirD = AVANCE;
				_CVitG = V3;
				_CVitD = V3;
				Etat = GV3;
				Mode = ACTIF;
				break;
			}
			case GV3: {
				_DirG = RECULE;
				_DirD = AVANCE;
				_CVitG = V3;
				_CVitD = V3;
				Etat = GV3;
				Mode = ACTIF;
				break;
			}
			}
			break;

		}
	}
}
}
void controle(void) {

	if (Tech >= T_200_MS) {
		Tech = 0;
		tmp_park += 200;
		PARK_ASSIST();
		ACS();
		Calcul_Vit();
		regulateur();
	}

}


void PARK_ASSIST()
{
	if(tmp_park>=tmp_seuil)
	{
		switch (etat_park)
		{
			case DEBUT:
			{
				break;
			}
			case ATTENTE_PARK:
			{
				break;
			}
			case AVANCER_MESURE:
			{
				_DirD=AVANCE;
				_DirG=AVANCE;
				_CVitD=V1;
				_CVitG=V1;
				tmp_seuil= 6000;
				etat_park = TOURNER_SERVO_m90;
				break;
			}
			case TOURNER_SERVO_m90:
			{
				_CVitD=0;
				_CVitG=0;
				tmp_seuil= T_TOURNER_SONAR;
				setAngleSonar(-90);
				etat_park = LANCEMENT_MESURE_Z;
				break;
			}
			case TOURNER_SERVO_p90:
			{
				tmp_seuil= T_TOURNER_SONAR;
				setAngleSonar(90);
				etat_park = LANCEMENT_MESURE_X;
				break;
			}
			case LANCEMENT_MESURE_X:
			{
				tmp_seuil=200;
				lecture_sonar();
				etat_park = ENREGISTREMENT_MESURE_X;
				break;
			}
			case LANCEMENT_MESURE_Z:
			{
				tmp_seuil=200;
				lecture_sonar();
				etat_park = ENREGISTREMENT_MESURE_Z;
				break;
			}
			case ENREGISTREMENT_MESURE_X:
			{
				tmp_seuil=0;
				if(nb_mesure<5)
				{
					mesures[nb_mesure]=dist_sonar;
					nb_mesure++;
					etat_park = LANCEMENT_MESURE_X;
				}
				else
				{
					nb_mesure=0;
					pos_x = Moyenne(mesures,5);
					etat_park = TOURNER_ROB;
				}
				break;
			}
			case ENREGISTREMENT_MESURE_Z:
			{
				tmp_seuil=0;
				if(nb_mesure<5)
				{
					mesures[nb_mesure]=dist_sonar;
					nb_mesure++;
					etat_park = LANCEMENT_MESURE_Z;
				}
				else
				{
					nb_mesure=0;
					pos_z = Moyenne(mesures,5);
					etat_park = TOURNER_SERVO_p90;
				}
				break;
			}
			case TOURNER_ROB:
			{

				if (pos_z>=target_z-MARGE_ALIGNEMENT-ERREUR_ALIGNEMENT && pos_z<=target_z-MARGE_ALIGNEMENT+ERREUR_ALIGNEMENT) // Robot deja align�
				{
					_CVitD = 0;
					_CVitG = 0;
					tmp_seuil = 0;
					etat_park = TOURNER_SERVO_0;
				}
				else
				{
					if (pos_z >= target_z)
					{
						sens=1;
						_DirD = AVANCE;
						_DirG = RECULE;

					}
					else
					{
						sens=0;
						_DirD = RECULE;
						_DirG = AVANCE;

					}
					_CVitD = V3;
					_CVitG = V3;
					tmp_seuil = T_TOURNER_90_1;
					etat_park = AVANCER_ALIGNEMENT;
				}
				break;
			}
			case AVANCER_ALIGNEMENT:
			{
				if (sens == 1) //On va vers les Z
				{
					tmp_seuil = abs(target_z - pos_z) + MARGE_Z;
				}
				else //On va vers les X
				{
					tmp_seuil = abs(target_z - pos_z - MARGE_X); //MARGE_X
				}

				_DirD=AVANCE;
				_DirG=AVANCE;
				_CVitD=V1;
				_CVitG=V1;
				etat_park = TOURNER_PLACE;
				break;
			}
			case TOURNER_PLACE:
			{
				if (sens == 1)
				{
					_DirD = RECULE;
					_DirG = AVANCE;
				}
				else
				{
					_DirD = AVANCE;
					_DirG = RECULE;
				}
				_CVitD = V3;
				_CVitG = V3;

				tmp_seuil = T_TOURNER_90_2;
				etat_park = TOURNER_SERVO_0;
				break;
			}
			case TOURNER_SERVO_0:
			{
				_CVitD = 0;
				_CVitG = 0;
				setAngleSonar(0);
				tmp_seuil = T_TOURNER_SONAR;
				etat_park = LANCEMENT_MESURE_Y;
				break;
			}
			case LANCEMENT_MESURE_Y:
			{
				tmp_seuil = 200;
				lecture_sonar();
				etat_park = ENREGISTREMENT_MESURE_Y;
				break;
			}
			case ENREGISTREMENT_MESURE_Y:
			{
				tmp_seuil = 0;
				if(nb_mesure<5)
				{
					mesures[nb_mesure]=dist_sonar;
					nb_mesure++;
					etat_park = LANCEMENT_MESURE_Y;
				}
				else
				{
					nb_mesure=0;
					pos_y = Moyenne(mesures,5);
					etat_park = AVANCER_PLACE;
				}
				break;
			}
			case AVANCER_PLACE:
			{
				_CVitD = V1;
				_CVitG = V1;
				_DirG = AVANCE;
				_DirD = AVANCE;
				tmp_seuil = (pos_y - MARGE_Y);
				etat_park = PARKING;
				break;
			}
			case PARKING:
			{
				tmp_seuil=0;
				CMDE=PARK;
				New_CMDE=1;
				break;
			}
			case TOURNER_SERVO_POS_Z:
			{
				_CVitD=0;
				_CVitG=0;
				tmp_seuil= T_TOURNER_SONAR;
				setAngleSonar(-90);
				etat_park = LANCEMENT_MESURE_POS_Z;
				break;
			}
			case LANCEMENT_MESURE_POS_Z:
			{
				tmp_seuil=200;
				lecture_sonar();
				etat_park = ENREGISTREMENT_MESURE_POS_Z;
				break;
			}
			case ENREGISTREMENT_MESURE_POS_Z:
			{
				tmp_seuil=0;
				if(nb_mesure<5)
				{
					mesures[nb_mesure]=dist_sonar;
					nb_mesure++;
					etat_park = LANCEMENT_MESURE_POS_Z;
				}
				else
				{
					nb_mesure=0;
					pos_z = Moyenne(mesures,5);
					etat_park = FIN;
				}
				break;
			}
			case FIN:
			{
				_CVitD = 0;
				_CVitG = 0;
				setAngleSonar(0);
				tmp_seuil=1000; //timeout pas d'adresse re�ue
				etat_park = PARKED;
				zig_tx_send('D');
				break;
			}
			case PARKED:
			{
				if (i_addr_rx != 0)
				{
					target_addr = addr_array[mini(addr_array, NB_ROBOT)];
					CMDE = SEND_MOV_PARK;
					New_CMDE = 1;
					i_addr_rx=0;
				}
				etat_park = REINIT;
				tmp_seuil = 400;
				break;
			}
			case REINIT:
			{
				etat_park = DEBUT;
				Mode=SLEEP;
				CMDE=START;
				New_CMDE=1;
				break;
			}
		}
		tmp_park=0;
	}

}

void ACS(void) {
	enum ETAT {
		ARRET, ACTIF
	};
	static enum ETAT Etat = ARRET;
	static uint16_t Delta1 = 0;
	static uint16_t Delta2 = 0;
	static uint16_t Delta3 = 0;
	static uint16_t Delta4 = 0;

	switch (Etat) {
	case ARRET: {
		if (Mode == ACTIF )
			Etat = ACTIF;
		else {
			CVitD = _CVitD;
			CVitG = _CVitG;
			DirD = _DirD;
			DirG = _DirG;
		}
		break;
	}
	case ACTIF: {
		if (Mode == SLEEP)
			Etat = ARRET;
		if (_DirD == AVANCE && _DirG == AVANCE) {
			if ((Dist_ACS_1 < Seuil_Dist_1 - Delta1)
					&& (Dist_ACS_2 < Seuil_Dist_2 - Delta2)) {
				CVitD = _CVitD;
				CVitG = _CVitG;
				DirD = _DirD;
				DirG = _DirG;
				Delta1 = Delta2 = 0;
			} else if ((Dist_ACS_1 < Seuil_Dist_1)
					&& (Dist_ACS_2 > Seuil_Dist_2)) {
				CVitD = V1;
				CVitG = V1;
				DirG = AVANCE;
				DirD = RECULE;
				Delta2 = DELTA;
			} else if ((Dist_ACS_1 > Seuil_Dist_1)
					&& (Dist_ACS_2 < Seuil_Dist_2)) {
				CVitD = V1;
				CVitG = V1;
				DirD = AVANCE;
				DirG = RECULE;
				Delta1 = DELTA;
			} else if ((Dist_ACS_1 > Seuil_Dist_1)
					&& (Dist_ACS_2 > Seuil_Dist_2)) {
				CVitD = 0;
				CVitG = 0;
				DirD = RECULE;
				DirG = RECULE;
			}
		} else if (_DirD == RECULE && _DirG == RECULE) {
			if ((Dist_ACS_3 < Seuil_Dist_3 - Delta3)
					&& (Dist_ACS_4 < Seuil_Dist_4 - Delta4)) {
				CVitD = _CVitD;
				CVitG = _CVitG;
				DirD = _DirD;
				DirG = _DirG;
				Delta3 = Delta4 = 0;
			} else if ((Dist_ACS_3 > Seuil_Dist_3)
					&& (Dist_ACS_4 < Seuil_Dist_4)) {
				CVitD = V1;
				CVitG = V1;
				DirD = AVANCE;
				DirG = RECULE;
				Delta3 = DELTA;
			} else if ((Dist_ACS_3 < Seuil_Dist_3)
					&& (Dist_ACS_4 > Seuil_Dist_4)) {
				CVitD = V1;
				CVitG = V1;
				DirG = AVANCE;
				DirD = RECULE;
				Delta4 = DELTA;
			} else if ((Dist_ACS_3 > Seuil_Dist_3)
					&& (Dist_ACS_4 > Seuil_Dist_4)) {
				CVitD = 0;
				CVitG = 0;
				DirD = RECULE;
				DirG = RECULE;
			}
		} else {
			CVitD = _CVitD;
			CVitG = _CVitG;
			DirD = _DirD;
			DirG = _DirG;
		}
		break;
	}
	}
}

void Calcul_Vit(void) {

	DistD = __HAL_TIM_GET_COUNTER(&htim3);
	DistG = __HAL_TIM_GET_COUNTER(&htim4);
	VitD = abs(DistD - DistD_old);
	VitG = abs(DistG - DistG_old);
	DistD_old = DistD;
	DistG_old = DistG;
	if (DirD == DirG) {
		Dist_parcours = Dist_parcours + ((VitD + VitG) >> 1);
	}
}

void regulateur(void) {
	enum ETAT {
		ARRET, ACTIF
	};
	static enum ETAT Etat = ARRET;
	uint16_t Kp_D = CKp_D;
	uint16_t Kp_G = CKp_G;
	uint16_t Ki_D = CKi_D;
	uint16_t Ki_G = CKi_G;
	uint16_t Kd_D = CKd_D;
	uint16_t Kd_G = CKd_G;

	static int16_t ErreurD = 0;
	static int16_t ErreurG = 0;
	static int16_t ErreurD_old = 0;
	static int16_t ErreurG_old = 0;
	static int16_t S_erreursD = 0;
	static int16_t S_erreursG = 0;
	static int16_t V_erreurD = 0;
	static int16_t V_erreurG = 0;

	switch (Etat) {
	case ARRET: {
		if (Mode == ACTIF)
			Etat = ACTIF;
		else {
			__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, 0);
			__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, 0);
			HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_4);
			HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_1);
			HAL_GPIO_WritePin(IR3_out_GPIO_Port, IR3_out_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(IR4_out_GPIO_Port, IR4_out_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(IR1_out_GPIO_Port, IR1_out_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(IR2_out_GPIO_Port, IR2_out_Pin, GPIO_PIN_RESET);

			HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON,
					PWR_SLEEPENTRY_WFI);

			__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, 0);
			__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, 0);
			HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_4);
			HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
			Time = 0;
		}
		break;
	}
	case ACTIF: {
		if ((CVitD != 0) && (CVitG != 0))
			Time = 0;
		if ((Mode == SLEEP) && (VitD == 0) && (VitG == 0) && Time > T_2_S)
			Etat = ARRET;
		else {
			ErreurD = CVitD - VitD;
			ErreurG = CVitG - VitG;
			S_erreursD += ErreurD;
			S_erreursG += ErreurG;
			V_erreurD = ErreurD - ErreurD_old;
			V_erreurG = ErreurG - ErreurG_old;
			ErreurD_old = ErreurD;
			ErreurG_old = ErreurG;
			Cmde_VitD = (unsigned int) Kp_D * (int) (ErreurD)
					+ (unsigned int) Ki_D * ((int) S_erreursD)
					+ (unsigned int) Kd_D * (int) V_erreurD;
			Cmde_VitG = (unsigned int) Kp_G * (int) (ErreurG)
					+ (unsigned int) Ki_G * ((int) S_erreursG)
					+ (unsigned int) Kd_G * (int) V_erreurG;

			//Cmde_VitD = _CVitD*640;
			//Cmde_VitG = _CVitG*640;
			//	DirD = _DirD;
			//	DirG= _DirG;

			if (Cmde_VitD < 0)
				Cmde_VitD = 0;
			if (Cmde_VitG < 0)
				Cmde_VitG = 0;
			if (Cmde_VitD > 100 * POURCENT)
				Cmde_VitD = 100 * POURCENT;
			if (Cmde_VitG > 100 * POURCENT)
				Cmde_VitG = 100 * POURCENT;
			__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, (uint16_t ) Cmde_VitG);
			__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, (uint16_t ) Cmde_VitD);
			HAL_GPIO_WritePin(DIR1_GPIO_Port, DIR1_Pin, (GPIO_PinState) DirD);
			HAL_GPIO_WritePin(DIR2_GPIO_Port, DIR2_Pin, (GPIO_PinState) DirG);

		}
		break;
	}
	}
}

void zig_tx_send(uint8_t buffer)
{
	HAL_StatusTypeDef status;
	status = HAL_UART_Transmit(&huart1, &buffer, sizeof(buffer), 10);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

	enum STATE_ZIGBEE {
		ATTENTE_RECEPTION,
		RECEPTION_ADDR,
		RECEPTION_POS,
		RECEPTION_MOV_PARK
	};
	static enum STATE_ZIGBEE state_zigbee = ATTENTE_RECEPTION;

	if (huart->Instance == USART3)
	{
		switch (BLUE_RX)
		{
			case 'F': {
				CMDE = AVANT;
				//New_CMDE = 1;
				break;
			}

			case 'B': {
				CMDE = ARRIERE;
				//New_CMDE = 1;
				break;
			}

			case 'L': {
				CMDE = GAUCHE;
				//New_CMDE = 1;
				break;
			}

			case 'R': {
				CMDE = DROITE;
				//New_CMDE = 1;
				break;
			}

			case 'D':{
				// disconnect bluetooth
				break;
			}
			case 'W' :
			{
				CMDE = PARK;
				New_CMDE = 1;
				break;
			}
			case 'X' :
			{
				CMDE = CMD_ATTENTE_PARK;
				New_CMDE = 1;
				break;
			}
			case 'U':
			{
				CMDE = STOP;
				New_CMDE = 1;
				break;
			}
			case 'u':
			{
				CMDE = START;
				New_CMDE = 1;
				break;
			}
			default:
				New_CMDE = 1;
		}
		HAL_UART_Receive_IT(&huart3, &BLUE_RX, 1);
	}
	else if (huart->Instance == USART1)
	{
		HAL_UART_Receive_IT(&huart1, &zigbee_RX, 1);
		if (etat_park == ATTENTE_PARK)
		{
			switch (state_zigbee)
			{
				case ATTENTE_RECEPTION:
				{
					if (zigbee_RX == 'D')
					{
						CMDE = DMD_ADDR;
						New_CMDE = 1;
						state_zigbee = RECEPTION_ADDR;
					}
					break;
				}
				case RECEPTION_ADDR:
				{
					if (zigbee_RX == ADDR)
					{
						state_zigbee = RECEPTION_POS;
					}
					else if (zigbee_RX == 'D')
					{
						CMDE = DMD_ADDR;
						New_CMDE = 1;
						state_zigbee = RECEPTION_ADDR;
					}
					else
					{
						state_zigbee = ATTENTE_RECEPTION;
						i_rx_pos = 0;
					}
					break;
				}
				case RECEPTION_POS:
				{
					switch(i_rx_pos)
					{
						case 0:
						{
							target_z=zigbee_RX;
							i_rx_pos++;
							break;
						}
						case 1:
						{
							target_z+=zigbee_RX*256;
							i_rx_pos++;
							break;
						}
						case 2:
						{
							target_z+=zigbee_RX*256*256;
							i_rx_pos=0;
							state_zigbee = RECEPTION_MOV_PARK;
							break;
						}
					}
					break;
				}
				case RECEPTION_MOV_PARK:
				{
					if (zigbee_RX == 'M')
					{
						CMDE = MOV_PARK;
						New_CMDE = 1;
						state_zigbee = ATTENTE_RECEPTION;
					}
					else
					{
						state_zigbee = ATTENTE_RECEPTION;
					}
					break;
				}
			}
		}
		else if (etat_park = PARKED)
		{
			if(zigbee_RX >= 1 && zigbee_RX <= 9 && i_addr_rx < NB_ROBOT)
			{
				addr_array[i_addr_rx] = zigbee_RX;
				i_addr_rx++;
			}
		}
	}
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc) {

	Dist_ACS_3 = adc_buffer[0] - adc_buffer[5];
	Dist_ACS_4 = adc_buffer[3] - adc_buffer[8];
	Dist_ACS_1 = adc_buffer[1] - adc_buffer[6];
	Dist_ACS_2 = adc_buffer[2] - adc_buffer[7];
	HAL_ADC_Stop_DMA(hadc);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef * htim) {
	static unsigned char cpt = 0;

	if ( htim->Instance == TIM2) {
		cpt++;
		Time++;
		Tech++;

		switch (cpt) {
		case 1: {
			HAL_GPIO_WritePin(IR3_out_GPIO_Port, IR3_out_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(IR4_out_GPIO_Port, IR4_out_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(IR1_out_GPIO_Port, IR1_out_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(IR2_out_GPIO_Port, IR2_out_Pin, GPIO_PIN_SET);
			break;
		}
		case 2: {
			HAL_ADC_Start_DMA(&hadc1, (uint32_t *) adc_buffer, 10);
			break;
		}
		case 3: {
			HAL_GPIO_WritePin(IR3_out_GPIO_Port, IR3_out_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(IR4_out_GPIO_Port, IR4_out_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(IR1_out_GPIO_Port, IR1_out_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(IR2_out_GPIO_Port, IR2_out_Pin, GPIO_PIN_RESET);
			break;
		}
		case 4: {
			HAL_ADC_Start_DMA(&hadc1, (uint32_t *) adc_buffer, 10);
			break;
		}
		default:
			cpt = 0;
		}
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {


	static unsigned char TOGGLE = 0;

	if (TOGGLE)
		CMDE = STOP;
	else
		CMDE = START;
	TOGGLE = ~TOGGLE;
	New_CMDE = 1;

}

void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{

	if ( htim->Instance == TIM1 )
	{
		//lecture de la valeur
		dist_sonar = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_2);
		//raz du gpio du trig
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, GPIO_PIN_RESET);
		//fin lecture a true
		fin_lect_sonar = 1;
	}
}


void lecture_sonar()
{
	//Trigger sonar
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, GPIO_PIN_SET);
	//Attendre 50ms
	fin_lect_sonar = 0;
}

void setAngleSonar(int angle)
{
	switch (angle)
	{
		case 90:
			htim1.Instance->CCR4 = 840;
			break;
		case 0:
			htim1.Instance->CCR4 = 2400;
			break;
		case -90:
			htim1.Instance->CCR4 = 4200;
			break;
		default:
			htim1.Instance->CCR4 = 2400;
			break;
	}
}

uint8_t mini(uint8_t* tab, uint8_t longueur)
{
	uint8_t minimum = 0;
	for(uint8_t i = 0; i<longueur; i++)
	{
		if (tab[i] < tab[minimum] && tab[i] != 0)
		{
			minimum = i;
		}
	}
	return minimum;
}

uint32_t Moyenne(uint32_t* tab, uint8_t taille)
{
	uint32_t res = 0;
	for(uint8_t i = 0; i<taille; i++)
	{
		res+=tab[i];
	}
	return res/taille;
}

void HAL_ADC_LevelOutOfWindowCallback(ADC_HandleTypeDef *hadc) {

	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);

}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
